#include <Nulleverse/Nulleverse.h>


int main(const int argc, const char* argv[])
{
    Nulleverse nulleverse;
    nulleverse.start();

    return 0;
}